# coding:utf8
import base64
from flask import Flask, request, jsonify
from threading import Timer
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = 'dev'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://wechat:123456@47.117.124.2:3306/wechat?charset=utf8'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app, use_native_unicode='utf8')
from models import *


@app.route('/passenger_signup', methods=['GET','POST'])
def passenger_signup() -> str:
    """Responds to get request for passenger registration.

    Return:
        '1': Success, '2': Server internal error, '4': Username already exists, '5': Id already exists.
    """
    username = request.form.get('userName')
    password = request.form.get('password')
    id_number = request.form.get('id')
    phone = request.form.get('phone')
    photo = request.form.get('photo')
    # 先实例化一个乘客对象
    instance_passenger = Passenger(p_id_num=id_number, p_name=username, p_pwd=generate_password_hash(password),
                                   p_phone=phone, p_photo_path='user_img/passenger_'+username+'.jpg')
    # 若学号/工号已经被注册，则返回状态码5
    if db.session.query(Passenger).filter_by(p_id_num=id_number).first():
        return '5'
    # 若用户名已存在，则返回状态码4
    elif db.session.query(Passenger).filter_by(p_name=username).first():
        return '4'
    # 输入的信息没问题
    try:
        # 在数据库中插入乘客的信息
        db.session.add(instance_passenger)
        db.session.commit()
        # 将图片存在本目录下的user_img文件夹内
        img = base64.b64decode(photo)
        with open('./user_img/passenger_' + username + '.jpg', 'wb') as file:
            file.write(img)
        # 向数据库中插入刚才已经创建好的用户对应的basic项
        passenger = db.session.query(Passenger).filter_by(p_name=username).one()
        try:
            instance_passenger_basic = PassengerBasic(p_id=passenger.id)
            db.session.add(instance_passenger_basic)
            db.session.commit()
        except Exception as e:
            with open('error.txt', 'a+') as fd:
                fd.write('%s\n' % e)
            db.session.rollback()
            return '2'
        return '1'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return '2'

@app.route('/driver_signup', methods=['GET','POST'])
def driver_signup():
    """Responds to get request for passenger registration.

    Return:
        '1': Success, '2': Server internal error, '4': Username already exists, '5': Id already exists.
    """
    username = request.form.get('userName')
    password = request.form.get('password')
    id_number = request.form.get('id')
    phone = request.form.get('phone')
    photo = request.form.get('photo')
    # 先实例化一个司机对象
    instance_driver = Driver(d_id_num=id_number, d_name=username, d_pwd=generate_password_hash(password),
                             d_phone=phone, d_photo_path='user_img/driver_'+username+'.jpg')
    # 若身份证已经被注册，则返回状态码5
    if db.session.query(Driver).filter_by(d_id_num=id_number).first():
        return '5'
    # 若用户名已存在，则返回状态码4
    elif db.session.query(Driver).filter_by(d_name=username).first():
        return '4'
    # 若司机输入的信息无误
    try:
        # 向数据库中插入司机的信息
        db.session.add(instance_driver)
        db.session.commit()
        # 将图片存在本目录下的user_img文件夹内
        img = base64.b64decode(photo)
        with open('./user_img/driver_' + username + '.jpg', 'wb') as file:
            file.write(img)
        # 向数据库中插入刚才已经创建好的司机对应的basic项
        driver = db.session.query(Driver).filter_by(d_name=username).one()
        try:
            instance_driver_basic = DriverBasic(d_id=driver.id)
            db.session.add(instance_driver_basic)
            db.session.commit()
        except Exception as e:
            with open('error.txt', 'a+') as fd:
                fd.write('%s\n' % e)
            db.session.rollback()
            return '2'
        return '1'
    except Exception as e:
        # 错误日志功能，若服务端出现错误，返回2
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return '2'

@app.route('/passenger_login', methods=['GET','POST'])
def passenger_login():
    """Passenger login."""
    username = request.form.get('userName')
    password = request.form.get('password')
    # 查找乘客
    tmp_p = db.session.query(Passenger).filter_by(p_name=username).first()
    # with open('debug.log', 'a+') as fd:
    #         fd.write(f'{check_password_hash(password, tmp_p.p_pwd)}\n')
    # 若在司机中能查找到且密码正确
    if tmp_p and check_password_hash(tmp_p.p_pwd, password):
        return {'status': 'ok', 'user_id': tmp_p.id}
    return 'wrong'

@app.route('/driver_login', methods=['GET','POST'])
def driver_login():
    """Driver login."""
    username = request.form.get('userName')
    password = request.form.get('password')
    # 查找司机
    tmp_d = db.session.query(Driver).filter_by(d_name=username).first()
    tmp_d_b = db.session.query(DriverBasic).filter_by(d_id=tmp_d.id).first()
    # 若在司机中能查找到且密码正确
    if tmp_d and tmp_d_b and check_password_hash(tmp_d.d_pwd, password):
        return {'status': 'ok', 'user_id': tmp_d.id, 'user_idle': tmp_d_b.d_idle}
    return 'wrong'

@app.route('/add_car', methods=['POST', 'GET','POST'])
def add_car():
    """Driver add car inrequest.formation.

    Return:
        '2': Car license exists, '1':Good.
    """
    user_id = request.args.get('user_id')
    brand = request.form.get('brand')
    color = request.form.get('password')
    car_license = request.form.get('licence')
    carPhoto = request.form.get('photo')
    # 先实例化一个car对象
    instance_car = Car(d_id=user_id, c_brand=brand, c_color=color, c_licence=car_license,
                       c_photo_path='car_img/car_' + car_license + '.jpg')
    # 若车牌号已经被注册，则返回状态码2
    if db.session.query(Car).filter_by(c_licence=car_license).first():
        return {'status': 2, 'user_id': user_id}
    # 若输入的信息无误
    try:
        db.session.add(instance_car)
        db.session.commit()
        # 将图片存在本目录下的user_img文件夹内
        img = base64.b64decode(carPhoto)
        with open('./car_img/car_' + car_license + '.jpg', 'wb') as file:
            file.write(img)
        return {'status': 1, 'user_id': user_id}
    except Exception as e:
        # 错误日志功能，若服务端出现错误，返回2
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return {'status': 2, 'user_id': user_id}

@app.route('/alter_car', methods=['GET','POST'])
def alter_car():
    """Driver alters car inrequest.formation."""
    car_id = request.args.get('car_id')
    brand = request.form.get('brand')
    color = request.form.get('color')
    photo = request.form.get('photo')
    old_car = db.session.query(Car).get(car_id)
    old_car.c_brand = brand
    old_car.c_color = color
    try:
        db.session.add(old_car)
        db.session.commit()
        # 将图片存在本目录下的user_img文件夹内
        img = base64.b64decode(photo)
        with open('./car_img/car_' + car_id + '.jpg', 'wb') as file:
            file.write(img)
        return {'status': 1, 'user_id': old_car.d_id}
    except Exception as e:
        # 错误日志功能，若服务端出现错误，返回2
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return {'status': 2, 'user_id': old_car.d_id}

@app.route('/requestDriverInfo', methods=['POST', 'GET','POST'])
def request_driver_info():
    """Request for driver's inrequest.formation."""
    driver_id = request.args.get('user_id')
    tmp_d = db.session.query(Driver).filter_by(id=driver_id).one()
    if tmp_d is None:
        return "wrong"
    else:
        with open(tmp_d.d_photo_path, 'rb') as file:
            img = base64.b64encode(file.read())
            img = img.decode()                      # byte2string
        return {'username': tmp_d.d_name, 'idNumber': tmp_d.d_id_num, 'phoneNumber': tmp_d.d_phone, 'idPhoto': img}

@app.route('/requestPassengerInfo', methods=['POST', 'GET','POST'])
def request_passenger_info():
    """Request for passenger's inrequest.formation."""
    passenger_id = request.args.get('user_id')
    tmp_d = db.session.query(Passenger).filter_by(id=passenger_id).one()
    if tmp_d is None:
        return "wrong"
    else:
        with open(tmp_d.p_photo_path, 'rb') as file:
            img = base64.b64encode(file.read())
            img = img.decode()                      # byte2string
        return {'username': tmp_d.p_name, 'idNumber': tmp_d.p_id_num, 'phoneNumber': tmp_d.p_phone, 'idPhoto': img}

@app.route('/requestCarInfo', methods=['POST', 'GET','POST'])
def request_car_info():
    """Request all cars inrequest.formation of driver.

    If driver has no car, returns None. Otherwise returns list of cars.
    """
    driver_id = request.args.get('user_id')
    my_cars = db.session.query(Car).filter_by(d_id=driver_id).all()
    if not my_cars:
        return {}
    all_cars = []
    for my_car in my_cars:
        with open(my_car.c_photo_path, 'rb') as file:
            img = base64.b64encode(file.read())
            img = img.decode()                      # byte2string
        all_cars.append({'car_id':my_car.id, 'car_color':my_car.c_color, 'car_name': my_car.c_brand, 'car_license': my_car.c_licence, 'car_photo': img})
    return jsonify({'all_cars': all_cars})

@app.route('/passenger_call', methods=['POST', 'GET','POST'])
def passenger_call():
    """Passenger booking a driver.

    Return:
        '0': No idle driver, '1': Book successfully,
        '2': Passenger is banned for 1 day, '3': Passenger is banned for 7 days.
    """
    route_id = request.form.get('routeIndex')
    r_time = request.form.get('date') + ' ' + request.form.get('endtime')
    p_id = request.args.get('user_id')
    user = db.session.query(PassengerBasic).filter_by(p_id=p_id).one()
    if user.p_status == 'ban1':
        return '2'
    elif user.p_status == 'ban7':
        return '3'
    # 在数据库中寻找状态为空闲的司机
    driver = db.session.query(DriverBasic).filter_by(d_idle='1', d_status='good').first()
    if not driver:
        return '0'
    # 在数据库订单信息中添加本此订单
    order = Reservation(r_id=route_id, p_id=p_id, d_id=driver.d_id, start_time=r_time, status='waited')
    # 同时修改司机的状态
    driver.d_idle = '0'
    try:
        db.session.add(order)
        db.session.add(driver)
        db.session.commit()
        return '1'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return '0'

@app.route('/myReservations', methods=['POST', 'GET','POST'])
def get_passenger_reservations():
    """Passenger searches for all his orders."""
    user_id = request.args.get('user_id')
    my_reservations = db.session.query(Reservation).filter_by(p_id=user_id).all()
    all_reservation = []
    for my_order in my_reservations:
        driver = db.session.query(Driver).filter_by(id=my_order.d_id).one()
        car = db.session.query(Car).filter_by(d_id=driver.id).one()
        route = db.session.query(Route).get(my_order.r_id)
        all_reservation.append({
            'order_id': my_order.id, 'generation_datetime': str(my_order.start_time),
            'state': my_order.status, 'src': route.r_start, 'dst': route.r_destination,
            'driver_name': driver.d_name, 'driver_phone': driver.d_phone,
            'car_brand': car.c_brand, 'car_color': car.c_color, 'car_licence': car.c_licence})
    return {'all_reservations': all_reservation}

@app.route('/diverReservations', methods=['POST', 'GET','POST'])
def get_driver_reservations():
    """Driver search for all his orders."""
    user_id = request.args.get('user_id')
    my_reservations = db.session.query(Reservation).filter_by(d_id=user_id).all()
    all_reservation = []
    for my_order in my_reservations:
        route = db.session.query(Route).get(my_order.r_id)
        all_reservation.append({'order_id': my_order.id, 'generation_datetime': str(my_order.start_time),
                                'state': my_order.status, 'src': route.r_start, 'dst': route.r_destination})
    return {'all_reservations': all_reservation}

@app.route('/driver_commit', methods=['POST', 'GET','POST'])
def driver_commit():
    """Driver confirm the arrival of passenger."""
    order_id = request.args.get('order_id')
    my_order = db.session.query(Reservation).get(order_id)
    my_order.status = 'on'
    try:
        db.session.add(my_order)
        db.session.commit()
        return '订单进行中'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return "发生意外"

@app.route('/driver_cancel', methods=['POST', 'GET','POST'])
def driver_cancel():
    """Driver cancels the order."""
    order_id = request.args.get('order_id')
    my_order = db.session.query(Reservation).get(order_id)
    my_order.status = 'canceled'
    try:
        db.session.add(my_order)
        db.session.commit()
        return '订单已取消'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return "发生意外"


@app.route('/driver_accept', methods=['POST', 'GET','POST'])
def driver_accept():
    """Driver login.

    Note that driver login means driver is idle. If driver has car and the status of driver is good,
    system can assign order to driver.
    """
    driver_id = request.args.get('user_id')
    if not db.session.query(Car).filter_by(d_id=driver_id).first():
        return "无法接单"
    my_list = db.session.query(DriverBasic).filter_by(d_id=driver_id).one()
    my_list.d_idle = '1'
    try:
        db.session.add(my_list)
        db.session.commit()
        return '司机开始接单'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return "发生意外"

@app.route('/driver_logout', methods=['POST', 'GET','POST'])
def driver_logout():
    """Driver logout."""
    driver_id = request.args.get('user_id')
    my_list = db.session.query(DriverBasic).filter_by(d_id=driver_id).one()
    my_list.d_idle = '0'
    try:
        db.session.add(my_list)
        db.session.commit()
        return '司机成功退出'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return "退出失败"

@app.route('/driver_complete', methods=['POST', 'GET','POST'])
def driver_complete():
    """Driver confirm the order is completed."""
    order_id = request.args.get('order_id')
    my_order = db.session.query(Reservation).get(order_id)
    my_order.status = 'complete'
    my_order.done_time = datetime.now().strftime("%Y-%m-%d %H:%M")
    driver = db.session.query(DriverBasic).filter_by(d_id=my_order.d_id).one()
    driver.d_idle = '1'
    try:
        db.session.add(my_order)
        db.session.add(driver)
        db.session.commit()
        return '订单完成'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return "发生意外"

@app.route('/driver_comment', methods=['POST', 'GET','POST'])
def driver_comment():
    """Driver looks at passenger's complaints and comments about himself."""
    user_id = request.args.get('user_id')
    my_reservations = db.session.query(Reservation).filter_by(d_id=user_id).all()
    my_status = db.session.query(DriverBasic).filter_by(d_id=user_id).one()
    all_reservation = []
    for my_order in my_reservations:
        if my_order.mark:
            all_reservation.append(f'来自乘客 {my_order.p_id} 的评价：{my_order.mark}')
        if my_order.comment:
            all_reservation.append(f'来自乘客 {my_order.p_id} 的投诉：{my_order.comment}')
    return {'all_marks_comments': all_reservation, 'driver_status': my_status.d_idle}

@app.route('/passenger_cancel', methods=['POST', 'GET','POST'])
def passenger_cancel():
    """Passenger cancels the order."""
    order_id = request.args.get('order_id')
    my_order = db.session.query(Reservation).get(order_id)
    if not my_order:
        return '该用户没有已存在的订单'
    try:
        # 修改订单的状态
        my_order.status = 'canceled'
        db.session.add(my_order)
        # 修改司机的状态
        driver = db.session.query(DriverBasic).filter_by(d_id=my_order.d_id).one()
        driver.status = '1'
        db.session.add(driver)
        db.session.commit()
        return '成功取消订单'
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return '发生错误，取消订单失败'

@app.route('/passenger_mark', methods=['POST', 'GET','POST'])
def passenger_mark():
    """Passenger comment for this order."""
    order_id = request.args.get('order_id')
    mark = request.form.get('myEvaluate')
    my_order = db.session.query(Reservation).get(order_id)
    if not my_order:
        return '该乘客没有订单'
    my_order.mark = mark
    try:
        db.session.add(my_order)
        db.session.commit()
        return {'user_id': my_order.p_id}
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return '发生错误，评价订单失败'

@app.route('/passenger_comment', methods=['POST', 'GET','POST'])
def passenger_comment():
    """Passenger complain about this order."""
    order_id = request.args.get('order_id')
    comment = request.form.get('myComplaint')
    my_order = db.session.query(Reservation).get(order_id)
    if not my_order:
        return '该乘客没有订单'
    my_order.comment = comment
    try:
        db.session.add(my_order)
        db.session.commit()
        return {'user_id': my_order.p_id}
    except Exception as e:
        with open('error.txt', 'a+') as fd:
            fd.write('%s\n' % e)
        db.session.rollback()
        return '发生错误，投诉订单失败'

def check_reservation():
    """The system periodically checks the order and bans the default passenger."""
    waited_lists = db.session.query(Reservation).filter_by(status='waited').all()
    for waited_list in waited_lists:
        # 计算时间差
        # start_time = datetime.strptime(waited_list.start_time, "%Y-%m-%d %H:%M")
        time_delta = datetime.now() - waited_list.start_time
        # 如果超时15min
        if time_delta.seconds > 900:
            # 让用户的处罚加倍并且更新黑名单
            user = db.session.query(PassengerBasic).filter_by(p_id=waited_list.p_id).one()
            if user.p_status != 'ban7':
                user.p_status = 'ban7'
                user.p_unlock_time = (datetime.now() + timedelta(days=7)).strftime("%Y-%m-%d %H:%M")
                db.session.add(user)
            # 将司机状态设置为空闲
            driver = db.session.query(DriverBasic).filter_by(d_id=waited_list.d_id).one()
            driver.d_idle = '1'
            # 设置订单状态为超时
            waited_list.status = 'timeout'
            try:
                db.session.add(driver)
                db.session.add(waited_list)
                db.session.commit()
            except Exception as e:
                with open('error.txt', 'a+') as fd:
                    fd.write('%s\n' % e)
                db.session.rollback()
        # 如果超过5min
        elif time_delta.seconds > 300:
            # ban 用户并将用户加入黑名单
            user = db.session.query(PassengerBasic).filter_by(p_id=waited_list.p_id, p_status='good').one()
            if not user:
                return
            user.p_status = 'ban1'
            user.p_unlock_time = (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d %H:%M")
            db.session.add(user)
            # black_list = BlackList(p_id=user.id, start_time=datetime.now().strftime("%Y-%m-%d %H:%M"),
            #                        end_time=(datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d %H:%M"),
            #                        level=user.status)
            # db.session.add(black_list)
            try:
                db.session.commit()
            except Exception as e:
                with open('error.txt', 'a+') as fd:
                    fd.write('%s\n' % e)
                db.session.rollback()
    # 每隔两分钟运行一次
    Timer(120, check_reservation).start()


check_reservation()

def check_blacklist():
    """Check whether the unbinding time is up."""
    black_lists = db.session.query(PassengerBasic).filter(PassengerBasic.p_status == 'ban1'
                                                       or PassengerBasic.p_status == 'ban7').all()
    cur_time = datetime.now()
    for black_list in black_lists:
        end_time = datetime.strptime(black_list.end_time, "%Y-%m-%d %H:%M")
        # 如果到了解绑的时间
        if cur_time > end_time:
            # 改变用户的状态
            black_list.p_status = 'good'
            try:
                db.session.add(black_list)
                db.session.commit()
            except Exception as e:
                with open('error.txt', 'a+') as fd:
                    fd.write('%s\n' % e)
                db.session.rollback()
    # 每两分钟运行一次
    Timer(120, check_blacklist).start()


check_blacklist()

if __name__ == '__main__':
    check_reservation()
    check_blacklist()
    app.run(host='127.0.0.77', port=5000, debug=True)
