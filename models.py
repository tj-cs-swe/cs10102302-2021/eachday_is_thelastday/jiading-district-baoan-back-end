# coding=utf8

""" Database models definition. """

from webserver import db


class Passenger(db.Model):
    """Table passenger store user-oriented information.

    Attributes:
        self.p_basic_id: Foreign key reference passenger_basic.id.
                        (passenger_basic is the extension of passenger)
    """
    __tablename__ = 'passenger'
    id = db.Column(db.Integer, primary_key=True)
    p_id_num = db.Column(db.String(7), unique=True, nullable=False)
    p_name = db.Column(db.String(20), unique=True, nullable=False)
    p_pwd = db.Column(db.String(255), nullable=False)
    p_phone = db.Column(db.String(11), nullable=False)
    p_photo_path = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return f'<Passenger: name={self.p_name}>'


class PassengerBasic(db.Model):
    """Table passenger_basic store basic system-oriented information.

    Attributes:
        self.p_status: ban1(passenger is banned for 1 day), ban7(passenger is banned for 7 days)
        self.p_unlock_time: Time when passenger is unbanned.
    """
    __tablename__ = 'passenger_basic'
    id = db.Column(db.Integer, primary_key=True)
    p_status = db.Column(db.Enum('good', 'ban1', 'ban7'), default='good')
    p_unlock_time = db.Column(db.DateTime)
    p_id = db.Column(db.Integer, db.ForeignKey('passenger.id'))

    def __repr__(self):
        return f'<PassengerBasic: id={self.p_id}, status={self.p_status}>'


class Driver(db.Model):
    """Table driver store user-oriented information."""
    __tablename__ = 'driver'
    id = db.Column(db.Integer, primary_key=True)
    d_id_num = db.Column(db.CHAR(18), unique=True, nullable=True)
    d_name = db.Column(db.VARCHAR(20), unique=True, nullable=True)
    d_pwd = db.Column(db.VARCHAR(255), nullable=True)
    d_phone = db.Column(db.CHAR(11), nullable=True)
    d_photo_path = db.Column(db.VARCHAR(255), nullable=True)

    def __repr__(self):
        return f'<Driver: name={self.d_name}>'


class DriverBasic(db.Model):
    """Table driver_basic store basic system-oriented information.

    Attributes:
        self.d_idle: '1'(driver is idle), '0'(driver is busy)
    """
    __tablename__ = 'driver_basic'
    id = db.Column(db.Integer, primary_key=True)
    d_id = db.Column(db.Integer, db.ForeignKey('driver.id'))
    d_status = db.Column(db.Enum('good', 'ban1', 'ban7'), default='good')
    d_unlock_time = db.Column(db.DateTime)
    d_idle = db.Column(db.CHAR(1), default='0')

    def __repr__(self):
        return f'<DriverBasic: id={self.d_id}, status={self.d_status}>'


class Car(db.Model):
    """Table car store the car information of all drivers.

    Traverse table to get all cars information specified by d_id.
    """
    __tablename__ = 'car'
    id = db.Column(db.Integer, primary_key=True)
    c_brand = db.Column(db.CHAR(255), nullable=True)
    c_color = db.Column(db.VARCHAR(20), nullable=True)
    c_licence = db.Column(db.VARCHAR(20), unique=True, nullable=True)
    c_photo_path = db.Column(db.VARCHAR(255), nullable=True)
    d_id = db.Column(db.Integer, db.ForeignKey('driver.id'))

    def __repr__(self):
        return f'<Car: brand={self.c_brand}, color={self.c_color}, license={self.c_licence}, driver_id={self.d_id}>'

class Route(db.Model):
    """Table route store all open routes."""
    __tablename__ = 'route'
    id = db.Column(db.Integer, primary_key=True)
    r_start = db.Column(db.CHAR(20), nullable=True)
    r_destination = db.Column(db.CHAR(20), nullable=True)

    def __repr__(self):
        return f'<Route {self.r_id}: {self.r_start} -> {self.r_destination}>'


class Reservation(db.Model):
    """Table reservation store information of all reservation.

    Attributes:
        self.mark: The mark given by passenger for this reservation.
        self.comment: Passenger complaint about driver.
        self.status: waited(driver is waiting for passenger), timeout(passenger didn't arrive at the appointed time),
                    canceled(canceled by driver/passenger), on(order in process), complete(order complete)
    """
    __tablename__ = 'reservation'
    id = db.Column(db.Integer, primary_key=True)
    p_id = db.Column(db.Integer, db.ForeignKey('passenger.id'))
    d_id = db.Column(db.Integer, db.ForeignKey('driver.id'))
    r_id = db.Column(db.Integer, db.ForeignKey('route.id'))
    start_time = db.Column(db.DateTime, nullable=True)
    done_time = db.Column(db.DateTime)
    mark = db.Column(db.String(255))
    comment = db.Column(db.String(255))
    status = db.Column(db.Enum('on', 'complete', 'canceled', 'waited', 'timeout'), nullable=True)

# def set_password(password):
#     return generate_password_hash(password)
#
# def check_password(password, password_hash):
#     return check_password_hash(password_hash, password)


# create all models defined above.
db.create_all()
