/*
Navicat MySQL Data Transfer

Source Server         : 微信约车小程序
Source Server Version : 50650
Source Host           : 47.117.124.2:3306
Source Database       : wechat

Target Server Type    : MYSQL
Target Server Version : 50650
File Encoding         : 65001

Date: 2021-08-19 23:02:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `c_brand` varchar(255) NOT NULL,
  `c_color` varchar(20) NOT NULL,
  `c_licence` varchar(20) NOT NULL,
  `c_photo_path` varchar(255) NOT NULL,
  `d_id` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `car_licence` (`c_licence`) USING BTREE,
  KEY `driver_id` (`d_id`) USING BTREE,
  CONSTRAINT `car_to_driverid` FOREIGN KEY (`d_id`) REFERENCES `driver` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of car
-- ----------------------------

-- ----------------------------
-- Table structure for driver
-- ----------------------------
DROP TABLE IF EXISTS `driver`;
CREATE TABLE `driver` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `d_name` varchar(20) NOT NULL,
  `d_pwd` varchar(255) NOT NULL,
  `d_id_num` char(18) NOT NULL,
  `d_phone` char(11) NOT NULL,
  `d_photo_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk_driver_id` (`id`) USING BTREE,
  UNIQUE KEY `driver_username` (`d_name`) USING BTREE,
  UNIQUE KEY `driver_identify_number` (`d_id_num`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of driver
-- ----------------------------

-- ----------------------------
-- Table structure for driver_basic
-- ----------------------------
DROP TABLE IF EXISTS `driver_basic`;
CREATE TABLE `driver_basic` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `d_id` int(8) NOT NULL,
  `d_status` enum('good','ban1','ban7') NOT NULL DEFAULT 'good',
  `d_unlock_time` datetime DEFAULT NULL,
  `d_idle` char(1) NOT NULL DEFAULT '0' COMMENT '''1'': idle, ''0'': busy',
  PRIMARY KEY (`id`),
  UNIQUE KEY `driver_id` (`d_id`) USING HASH,
  CONSTRAINT `driverbasic_to_driverid` FOREIGN KEY (`d_id`) REFERENCES `driver` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of driver_basic
-- ----------------------------

-- ----------------------------
-- Table structure for passenger
-- ----------------------------
DROP TABLE IF EXISTS `passenger`;
CREATE TABLE `passenger` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(20) NOT NULL,
  `p_pwd` varchar(255) NOT NULL,
  `p_id_num` varchar(7) NOT NULL,
  `p_phone` char(11) NOT NULL,
  `p_photo_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk_passenger_id` (`id`) USING HASH,
  UNIQUE KEY `passenger_username` (`p_name`) USING HASH,
  UNIQUE KEY `passenger_identify_number` (`p_id_num`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of passenger
-- ----------------------------

-- ----------------------------
-- Table structure for passenger_basic
-- ----------------------------
DROP TABLE IF EXISTS `passenger_basic`;
CREATE TABLE `passenger_basic` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `p_status` enum('good','ban1','ban7') DEFAULT 'good',
  `p_unlock_time` datetime DEFAULT NULL,
  `p_id` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `passengerbasic_to_passengerid` (`p_id`) USING BTREE,
  CONSTRAINT `passengerbasic_to_passengerid` FOREIGN KEY (`p_id`) REFERENCES `passenger` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of passenger_basic
-- ----------------------------

-- ----------------------------
-- Table structure for reservation
-- ----------------------------
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(8) DEFAULT NULL,
  `d_id` int(8) DEFAULT NULL,
  `r_id` int(8) DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `done_time` datetime DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` enum('on','complete','canceled','waited','timeout') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_to_driverid` (`d_id`),
  KEY `revervation_to_passengerid` (`p_id`),
  KEY `reservation_to_routeid` (`r_id`),
  CONSTRAINT `reservation_to_driverid` FOREIGN KEY (`d_id`) REFERENCES `driver` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `reservation_to_routeid` FOREIGN KEY (`r_id`) REFERENCES `route` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `revervation_to_passengerid` FOREIGN KEY (`p_id`) REFERENCES `passenger` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reservation
-- ----------------------------

-- ----------------------------
-- Table structure for route
-- ----------------------------
DROP TABLE IF EXISTS `route`;
CREATE TABLE `route` (
  `id` int(8) NOT NULL,
  `r_start` varchar(20) NOT NULL,
  `r_destination` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of route
-- ----------------------------
INSERT INTO `route` VALUES ('0', '同济大学嘉定校区', '昌吉东路地铁站');
INSERT INTO `route` VALUES ('1', '同济大学嘉定校区', '上海汽车城地铁站');
INSERT INTO `route` VALUES ('2', '同济大学嘉定校区', '安亭地铁站');
INSERT INTO `route` VALUES ('3', '同济大学嘉定校区', '嘉定新城地铁站');
INSERT INTO `route` VALUES ('4', '同济大学嘉定校区', '上海虹桥站');
INSERT INTO `route` VALUES ('5', '同济大学嘉定校区', '上海虹桥国际机场');
INSERT INTO `route` VALUES ('6', '同济大学嘉定校区', '同济大学四平校区');
INSERT INTO `route` VALUES ('7', '同济大学嘉定校区', '同济大学沪西校区');
INSERT INTO `route` VALUES ('8', '同济大学嘉定校区', '同济大学沪北校区');
INSERT INTO `route` VALUES ('9', '昌吉东路地铁站', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('10', '上海汽车城地铁站', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('11', '安亭地铁站', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('12', '嘉定新城地铁站', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('13', '上海虹桥站', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('14', '上海虹桥国际机场', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('15', '同济大学四平校区', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('16', '同济大学沪西校区', '同济大学嘉定校区');
INSERT INTO `route` VALUES ('17', '同济大学沪北校区', '同济大学嘉定校区');
DROP TRIGGER IF EXISTS `trigger_autosetid_d`;
DELIMITER ;;
CREATE TRIGGER `trigger_autosetid_d` BEFORE INSERT ON `driver` FOR EACH ROW BEGIN
 declare num int;
 select count(*) into num from driver;
 set NEW.id=concat('d',lpad(num,7,0));
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_autosetid_p`;
DELIMITER ;;
CREATE TRIGGER `trigger_autosetid_p` BEFORE INSERT ON `passenger` FOR EACH ROW BEGIN
 declare num int;
 select count(*) into num from passenger;
 set NEW.id=concat('p',lpad(num,7,0));
END
;;
DELIMITER ;
